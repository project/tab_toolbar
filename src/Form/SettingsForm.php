<?php

namespace Drupal\tab_toolbar\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'tab_toolbar.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tab_toolbar_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('tab_toolbar.settings');

    $form['admin'] = [
      '#type' => 'details',
      '#title' => $this->t('Admin'),
      '#open' => TRUE,
    ];

    $form['admin']['admin__enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable tab toolbar on admin theme'),
      '#description' => $this->t('Should the toolbar tab be enabled on the admin theme'),
      '#default_value' => $config->get('admin.enabled'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('tab_toolbar.settings');

    $config->set('admin.enabled', $form_state->getValue('admin__enabled'));

    $config->save();
  }

}
